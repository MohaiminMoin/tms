﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TMS.DLL.Models;

namespace TMS.ContextFolder
{
    public partial class TMSContext : DbContext
    {

        //static TMSContext()
        //{
        //    Database.SetInitializer<TMSContext>(null);
        //}

        public TMSContext() : base("Name=TMSContext")
        {
        }

        public virtual List<APKVersionInfoBO> APKVersionLatestList(string AppCode, string DeviceType)
        {
            object[] parameters = { AppCode, DeviceType };
            using (var db = new TMSContext())
            {
                return db.Database.SqlQuery<APKVersionInfoBO>("EXEC Tms.APKVersionGetLatest {0},{1}", parameters).ToList();
            }
        }

        public virtual int ApkDownloadHist_Add(ApkDownloadHistoryBO aHistory)
        {
            object[] parameters = { aHistory.CreatedBy, aHistory.RequestID, aHistory.MID, aHistory.TID, aHistory.AppCode, aHistory.AppVersionNo, aHistory.DeviceType, aHistory.DeviceSerialNo, aHistory.IMEI, aHistory.FileName, aHistory.DownloadURL };
            using (var db = new TMSContext())
            {
                return db.Database.SqlQuery<int>("declare @ErrTicket int, @PkId int ; exec  @ErrTicket = TMS.APKDownloadHist_Add {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}, @PkId = @PkId OUTPUT; select @ErrTicket", parameters).SingleOrDefault();
            }
        }
        

    }
}