﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.DLL.Models
{
    class Extra
    {
        public static readonly HashSet<string> OptionalError = new HashSet<string>
        {
            "0", "201"
        };
    }

    public class RespondModel
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public bool Status
        {
            get
            {
                if (Extra.OptionalError.Contains(ErrorCode.ToString())) return true;
                else return false;
            }
            set { }
        }
        public string Token { get; set; }
        public string MobileNo { get; set; }
        public string HostTxnId { get; set; }
        public string DownloadURL { get; set; }
    }

   
}
