﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.DLL.Models
{
    public class APKVersionInfoBO
    {
        public int PkId { get; set; }
        public string AppCode { get; set; }
        public string AppVersionNo { get; set; } // app version
        public string DeviceType { get; set; }
        public string FileName { get; set; }
        public string DownloadURL { get; set; }
        public DateTime? PublishDate { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceIMEI { get; set; }
        public int xIsDeleted { get; set; }       
    }
}