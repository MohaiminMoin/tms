﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.DLL.Models
{
    public class UserRegModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string MobileNo { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string DeviceId { get; set; }
        [Required]
        public string DeviceOs { get; set; }
        [Required]
        public string DeviceBrand { get; set; }
        [Required]
        public string DeviceModel { get; set; }
        [Required]
        public string AppVersion { get; set; }
        [Required]
        public string OTPTxnID { get; set; }
        [Required]
        public string OTPCode { get; set; }
    }
}
