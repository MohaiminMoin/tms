﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using TMS.ContextFolder;
using TMS.DLL.Models;

namespace TMS.Controllers
{
    public class VersionController : ApiController
    {
        private RespondModel respondModel = new RespondModel();        
        readonly TMSContext _tMSContext = new TMSContext();


        //[Authorize]
        [HttpPost]
        [Route("api/Version/CheckVersion")]
        public IHttpActionResult CheckVersion()
        {
           
            if (User.Identity.IsAuthenticated)
            {
                //.. Here we can find the claims detail which we used for token generation..
                //var identity = User.Identity as ClaimsIdentity;
                //if (identity != null)
                //{
                //    IEnumerable<Claim> claims = identity.Claims;
                //}


                var versionFormInfo = HttpContext.Current.Request.Form;               
               
                try
                {
                    var appCode = versionFormInfo["AppCode"].ToUpper();
                    var appVersionNo = versionFormInfo["AppVersionNo"].ToUpper();
                    var deviceType = versionFormInfo["DeviceType"].ToUpper();
                    //var publishDate = Convert.ToDateTime(versionFormInfo["PublishDate"]).Date;                                       

                    var versionDetail = _tMSContext.APKVersionLatestList(appCode, deviceType).SingleOrDefault();
                    if (versionDetail == null)
                    {
                        respondModel = new RespondModel { ErrorCode = 4000, Message = "APK version data not found" };
                        return Ok(respondModel);
                    }

                    if(versionDetail.AppVersionNo.ToUpper() != appVersionNo)
                    {
                        respondModel = new RespondModel { ErrorCode = 0, Message = "New Version Available", DownloadURL = versionDetail.DownloadURL };
                        return Ok(respondModel);
                    }
                }
                catch (Exception ex)
                {
                    respondModel = new RespondModel { ErrorCode = 4000, Message = ex.Message };
                }

            }
            else
            {
                respondModel = new RespondModel { ErrorCode = 4000, Message = "Authentication Failed" };
            }
            return Ok(respondModel);
        }


        [HttpPost]
        [Route("api/Version/UpdateHistory")]
        public IHttpActionResult UpdateHistory()
        {
            if (User.Identity.IsAuthenticated)
            {
                var history = HttpContext.Current.Request.Form;               

                try
                {
                    ApkDownloadHistoryBO aHistory = new ApkDownloadHistoryBO();
                    aHistory.CreatedBy =history["CreatedBy"];
                    aHistory.RequestID = Convert.ToInt32(history["RequestID"]);
                    aHistory.MID = history["MID"];
                    aHistory.TID = history["TID"];
                    aHistory.AppCode = history["AppCode"];
                    aHistory.AppVersionNo = history["AppVersionNo"];
                    aHistory.DeviceType = history["DeviceType"];
                    aHistory.DeviceSerialNo = history["DeviceSerialNo"];
                    aHistory.IMEI = history["IMEI"];
                    aHistory.FileName = history["FileName"];
                    aHistory.DownloadURL = history["DownloadURL"];

                    var insertDownloadHistory = _tMSContext.ApkDownloadHist_Add(aHistory);
                    if(insertDownloadHistory == 0)
                    {
                        return Ok(new RespondModel { ErrorCode = 0, Message = "Insert Successfull" });
                    }
                    else
                    {
                        return Ok(new RespondModel { ErrorCode = 4000, Message = "Insertion Failed" });
                    }

                }
                catch (Exception ex)
                {
                    respondModel = new RespondModel { ErrorCode = 4000, Message = ex.Message };
                }

            }
            else
            {
                respondModel = new RespondModel { ErrorCode = 4000, Message = "Authentication Failed" };
            }
            return Ok(respondModel);
        }


        #region Test

        //[Authorize]
        //[HttpPost]
        //public Object GetName2()
        //{
        //    var identity = User.Identity as ClaimsIdentity;
        //    if (identity != null)
        //    {
        //        IEnumerable<Claim> claims = identity.Claims;
        //        var name = claims.Where(p => p.Type == "Username").FirstOrDefault()?.Value;
        //        return new
        //        {
        //            data = name
        //        };
        //    }
        //    return null;
        //}

        //[HttpPost]
        //public String GetName1()
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        var identity = User.Identity as ClaimsIdentity;
        //        if (identity != null)
        //        {
        //            IEnumerable<Claim> claims = identity.Claims;
        //        }
        //        return "Valid";
        //    }
        //    else
        //    {
        //        return "Invalid";
        //    }
        //}

        #endregion


    }
}
