﻿using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;
using TMS.DLL.Models;

namespace TMS.Controllers
{
    public class GenerateTokenController : ApiController
    {
        private RespondModel respondModel = new RespondModel();

        [HttpPost]
        [Route("api/GenerateToken/GetToken")]
        public IHttpActionResult GetToken()
        {
            var userInfotxt = HttpContext.Current.Request.Form;

            string key = ConfigurationManager.AppSettings["secret"]; // "my_secret_key_12345"; //Secret key which will be used later during validation    
            var issuer = ConfigurationManager.AppSettings["issuer"];
            var audienceIssuer = ConfigurationManager.AppSettings["audienceIssuer"];

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            //permClaims.Add(new Claim("valid", "1"));
            permClaims.Add(new Claim("Username", userInfotxt["Username"].ToUpper()));
            permClaims.Add(new Claim("Password", userInfotxt["Password"].ToUpper()));

            try {
                var token = new JwtSecurityToken(issuer, //Issure    
                           audienceIssuer,  //Audience    
                           permClaims,
                           expires: DateTime.Now.AddDays(1),
                           signingCredentials: credentials);
                var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

                respondModel = new RespondModel { ErrorCode = 0, Message = "No Error", Token = jwt_token.ToString() };
            }
            catch (Exception ex)
            {
                respondModel = new RespondModel { ErrorCode =1000, Message = "Token Generation not succed" };
            }
            //Create Security Token object by giving required parameters    

            return Ok(respondModel);
            //return new { data = jwt_token };
        }




        //[HttpGet]
        //[Route("api/GenerateToken/GetToken11")]
        //public IHttpActionResult GetToken11()
        //{
        //    string key = ConfigurationManager.AppSettings["secret"]; // "my_secret_key_12345"; //Secret key which will be used later during validation    
        //    var issuer = ConfigurationManager.AppSettings["issuer"];
        //    var audienceIssuer = ConfigurationManager.AppSettings["audienceIssuer"];

        //    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
        //    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        //    //Create a List of Claims, Keep claims name short    
        //    var permClaims = new List<Claim>();
        //    permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
        //    //permClaims.Add(new Claim("valid", "1"));
        //    permClaims.Add(new Claim("Username", "aa"));
        //    permClaims.Add(new Claim("Password", "123"));

        //    try
        //    {
        //        var token = new JwtSecurityToken(issuer, //Issure    
        //                   audienceIssuer,  //Audience    
        //                   permClaims,
        //                   expires: DateTime.Now.AddDays(1),
        //                   signingCredentials: credentials);
        //        var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

        //        respondModel = new RespondModel { ErrorCode = 0, Message = "No Error", Token = jwt_token.ToString() };
        //    }
        //    catch (Exception ex)
        //    {
        //        respondModel = new RespondModel { ErrorCode = 1000, Message = "Token Generation not succed" };
        //    }
        //    //Create Security Token object by giving required parameters    

        //    return Ok(respondModel);
        //    //return new { data = jwt_token };
        //}


    }
}
